
<?php
session_start();
require_once("connect.php");

if (isset($_POST['submit'])) {
  $username = $_POST["email"];
  $password = $_POST["password"];
  $sql = "SELECT * FROM login WHERE username = '$username' AND password = '$password'";
  $result = mysqli_query($conn, $sql);

  if (mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_array($result);
    $_SESSION["user"] = $row['username'];
    
    if ($row['role'] == 'Customer') {
      $_SESSION["customer"] = true;
      // $_SESSION['customer'] = "Customer";
    } elseif ($row['role'] == 'Seller') {
      $_SESSION["seller"] = true; 
    } elseif ($row['role'] == 'Admin') {
      $_SESSION["admin"] = true;
    }
    
    header("Location: index.php");
    exit();
  } else {
    $_SESSION["error_message"] = "Incorrect username or password";
    header("Location: login.php");
    exit();
  }
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Signin Template · Bootstrap v5.0</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sign-in/">

    

    <!-- Bootstrap core CSS -->
<link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
    
<main class="form-signin">
  <center>
      <form style="margin-top:9rem; width:30%;" method=POST>
      <!-- <img class="mb-4" src="../assets/brand/bootstrap-logo.svg" alt="" width="72" height="57"> -->
      <h1 class="h3 mb-3 fw-normal">Please Login</h1>

      <div class="form-floating mb-3" >
        <input type="email" class="form-control" id="floatingInput" required placeholder="name@example.com" name="email">
        <label for="floatingInput">Email address</label>
      </div>
      <div class="form-floating">
        <input type="password" class="form-control" id="floatingPassword" required placeholder="Password" name="password">
        <label for="floatingPassword">Password</label>
      </div>

      <div class="checkbox mb-3 mt-2">
        <label>
          <a href="register.php">Create account</a>
          <!-- <input type="checkbox" value="remember-me"> Remember me -->
        </label>
      </div>
      <button class="w-100 btn btn-lg btn-primary" type="submit" name="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2017–2021</p>
    </form>
  </center>
 
</main>


    
  </body>
</html>
