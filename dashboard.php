<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
  <div class="position-sticky pt-3">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="index.php">
          <span data-feather="home"></span>
          Dashboard
        </a>
      </li>
      <?php
        if(isset($_SESSION['admin'])) {
          // Admin is logged in
      ?>
          <li class="nav-item">
            <a class="nav-link" href="product.php">
              <span data-feather="shopping-cart"></span>
              View Products
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="order.php">
              <span data-feather="users"></span>
              Orders
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="customer.php">
              <span data-feather="users"></span>
              Manage users
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about.php">
              <span data-feather="file"></span>
              About us
            </a>
          </li>
      <?php
        } else if(isset($_SESSION['seller'])) {
          // Seller is logged in
      ?>

          <li class="nav-item">
            <a class="nav-link" href="order.php">
              <span data-feather="users"></span>
              Orders
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="product.php">
              <span data-feather="shopping-cart"></span>
              Products
            </a>
          </li>
       <?php
        } else if(isset($_SESSION['customer'])) {
          // Customer is logged in
        ?>
          <li class="nav-item">
            <a class="nav-link" href="product.php">
              <span data-feather="shopping-cart"></span>
              View Products
            </a>
          </li>
      <?php
        }
      ?>
    </ul>
  </div>
</nav>