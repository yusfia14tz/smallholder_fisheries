<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Signin Template · Bootstrap v5.0</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sign-in/">

    

    <!-- Bootstrap core CSS -->
<link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
    
<main class="form-signin">
  <center>
      <form style="margin-top:9rem; width:30%;" action="register-handler.php" method="POST">
      <!-- s<img class="mb-4" src="../assets/brand/bootstrap-logo.svg" alt="" width="72" height="57"> -->
      <h1 class="h3 mb-3 fw-normal">Please register here</h1>

      <div class="form-floating mb-3" >
        <input type="text" class="form-control" name="first" id="floatingInput" required placeholder="name@example.com">
        <label for="floatingInput">First name</label>
      </div>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" name="last" id="floatingPassword" required placeholder="Password">
        <label for="floatingPassword">Last name</label>
      </div>
      <div class="form-floating mb-3" >
        <input type="text" class="form-control" name="email" id="floatingInput" required placeholder="name@example.com">
        <label for="floatingInput">Email address</label>
      </div>
      <div class="form-floating">
        <input type="text" class="form-control" name="phone" id="floatingPassword" required placeholder="Password">
        <label for="floatingPassword">Phone number</label>
      </div>
      <input type="submit" value="Register" name="send" class="w-100 btn btn-lg btn-primary">
      <div class="checkbox mb-3 mt-2" >
        <label>
            <a href="login.php">Go back</a>
          <!-- <input type="checkbox" value="remember-me"> Remember me -->
        </label>
      </div>
      <p class="mt-5 mb-3 text-muted">&copy; 2017–2021</p>
    </form>
  </center>
 
</main>


    
  </body>
</html>
